﻿using MongoDB.Bson;
using MongoDB.Driver;
using SparkHireUpdateData.Models.MOTU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Services
{
    public class VideoInterviewService
    {
        private readonly IMongoCollection<VideoInterview> _videoInterviews;

        public VideoInterviewService(IMOTUMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _videoInterviews = database.GetCollection<VideoInterview>(settings.VideoInterviewCollectionName);
        }

        public VideoInterview GetUuid(string integrationInterviewId)
        {
            
            var videoInterview =_videoInterviews.Find(v => v.IntegrationInterviewId == integrationInterviewId && v.Deleted == false).FirstOrDefault();
            return videoInterview;
        }
        //public List<Book> Get() =>
        //     _books.Find(book => true).ToList();

        //public Book Get(string id) =>
        //    _books.Find<Book>(book => book.Id == id).FirstOrDefault();

        //public Book Create(Book book)
        //{
        //    _books.InsertOne(book);
        //    return book;
        //}

        //public void Update(string id, Book bookIn) =>
        //    _books.ReplaceOne(book => book.Id == id, bookIn);

        //public void Remove(Book bookIn) =>
        //    _books.DeleteOne(book => book.Id == bookIn.Id);

        //public void Remove(string id) =>
        //    _books.DeleteOne(book => book.Id == id);
    }
}
