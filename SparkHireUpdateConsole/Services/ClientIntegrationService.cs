﻿using MongoDB.Driver;
using SparkHireUpdateData.Models.MOTU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Services
{
    public class ClientIntegrationService
    {
        private readonly IMongoCollection<ClientIntegration> _clientIntegrations;

        public ClientIntegrationService(IMOTUMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _clientIntegrations = database.GetCollection<ClientIntegration>(settings.ClientIntegrationCollectionName);
        }

        public List<ClientIntegration> GetAllSparkHireClients()
        {
            return _clientIntegrations.Find<ClientIntegration>(c => c.IntegrationId == "ec2729ad-6870-4d8f-9a01-5998bc232917" && c.Deleted == false).ToList();
        }
    }
}
