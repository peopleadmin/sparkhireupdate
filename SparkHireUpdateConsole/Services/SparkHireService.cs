﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SparkHireUpdateData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Services
{
    public class SparkHireService
    {
        private readonly ILogger<SparkHireService> _logger;

        //private readonly string _uuid;
        //private readonly string _apiKey;

        //public SparkHireService(string Uuid, string ApiKey)
        //{
        //    _uuid = Uuid;
        //    _apiKey = ApiKey;
        //}
        public SparkHireService(ILogger<SparkHireService> logger)
        {
            _logger = logger;
        }

        public async Task<VideoInterviewResult> GetStatusFromSparkHireAsync(string uuid, string apiKey)
        {
            var videoInterviewResult = new VideoInterviewResult();
            var accessToken = $"Basic {Convert.ToBase64String(Encoding.Default.GetBytes($"{apiKey}:"))}";
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization",accessToken);

            _logger.LogInformation($"API call: https://api.sparkhire.com/v1.0/interviews/{uuid} , accessToken: {accessToken}");
            using (var response = await httpClient.GetAsync($"https://api.sparkhire.com/v1.0/interviews/{uuid}"))
            {
                var apiResponse = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"API Response: {apiResponse}");

                JObject o = JObject.Parse(apiResponse);
                videoInterviewResult.Status = o["status"]?.ToString();

                string completedDate = o["completed_at"]?.ToString();
                //DateTime completedDateDate = DateTime.ParseExact(completedDate, "yyyy-MM-dd HH:mm:ss.fff", null);
                DateTime.TryParse(completedDate, out DateTime completedDateDate);
                if (completedDateDate.Year==1)
                {
                    videoInterviewResult.CompletedDate = null;
                }
                else
                {
                    videoInterviewResult.CompletedDate = completedDateDate;
                }
            }

            return videoInterviewResult;
        }
    }
}
