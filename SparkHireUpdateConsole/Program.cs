﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;
using SparkHireUpdateData.Entities.ATS;
using SparkHireUpdateData.Models.MOTU;
using SparkHireUpdateData.Persistence;
using SparkHireUpdateData.Persistence.Repositories;
using SparkHireUpdateData.Services;
using System;
using System.Threading.Tasks;

namespace SparkHireUpdateConsole
{
    class Program
    {
        static async Task Main(string[] args)
        {
            bool result = false;
            string currentDateTime = DateTime.Now.ToString("yyyyMMdd_HHmm");
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console()
                    .WriteTo.File($"C:\\JagsSparkHire\\Logs_{currentDateTime}.log")
                    .CreateLogger();

           //         .WriteTo.File($"C:\\JagsSparkHire\\Logs_{currentDateTime}.log")

            IConfiguration configuration = new ConfigurationBuilder()
                           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddEnvironmentVariables()
                           .AddCommandLine(args)
                           .Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, configuration);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            var logger = serviceProvider.GetService<ILogger<Program>>();
            try
            {

                Console.Write("Enter the days count: ");
                int pastDaysCount = Convert.ToInt32(Console.ReadLine());
                logger.LogInformation($"BEGINNING INTERVIEW STATUS UPDATE SINCE: {DateTime.Now.AddDays(-pastDaysCount)}");
                result = await serviceProvider.GetService<SparkHireUpdate>().UpdateSparkHireDataAsync(pastDaysCount);
                logger.LogInformation("THE END!");
            }
            catch (Exception)
            {
                logger.LogError("ERROR OCCURRED!");
            }
            if (result)
            {
                Console.WriteLine("COMPLETED SUCCESSFULLY!");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("FAILED!");
                Console.ReadKey();
            }
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //we will configure logging here
            services.AddLogging(configure => configure.AddSerilog())
                    .AddTransient<SparkHireUpdate>();

            services.Configure<MOTUMongoDatabaseSettings>(
                        configuration.GetSection(nameof(MOTUMongoDatabaseSettings)));

            services.AddSingleton<IMOTUMongoDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<MOTUMongoDatabaseSettings>>().Value);

            //services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IClientDetailRepository, ClientDetailRepository>();

            services.AddSingleton<ClientIntegrationService>();
            services.AddSingleton<VideoInterviewService>();
            services.AddSingleton<SparkHireService>();

            services.AddDbContext<OnDemandDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("OnDemandContext")));

            services.AddDbContext<AppDbContext>(
                         options => options.UseSqlServer("name=ConnectionStrings:DefaultConnection"));
            //services.AddScoped<IVideoInterviewRepository, VideoInterviewRepository>();
        }
    }

    public class SparkHireUpdate
    {
        private readonly ILogger<SparkHireUpdate> _logger;
        private readonly VideoInterviewService _videoInterviewService;
        private readonly ClientIntegrationService _clientIntegrationService;
        private readonly IClientDetailRepository _clientPreferencesRepository;
        private readonly SparkHireService _sparkHireService;

        public SparkHireUpdate(ILogger<SparkHireUpdate> logger,
            VideoInterviewService videoInterviewService,
            ClientIntegrationService clientIntegrationService,
            IClientDetailRepository clientPreferencesRepository,
            SparkHireService sparkHireService)
        {
            _logger = logger;
            _videoInterviewService = videoInterviewService;
            _clientIntegrationService = clientIntegrationService;
            _clientPreferencesRepository = clientPreferencesRepository;
            _sparkHireService = sparkHireService;
        }

        public async Task<bool> UpdateSparkHireDataAsync(int pastDaysCount)
        {
            try
            {
                _logger.LogInformation($"Getting clients with active SparkHire integration");

                var clientIntegrations = _clientIntegrationService.GetAllSparkHireClients();
                _logger.LogInformation($"SparkHire Clients count: {clientIntegrations.Count}");

                int countCI = 0;

                //Get LIST MOTUClientID and APIkey from MOTU Mongo
                foreach (var clientIntegration in clientIntegrations)
                {
                    _logger.LogInformation($"Processing starts({++countCI}/{clientIntegrations.Count}) for MOTUClientID: {clientIntegration.MOTUClientId}");

                    //Get ATS conn strings from ClientPref from ONDEMAND, using MOTU_ClientID. Usuaully this shud be only 1 records
                    var clients = await _clientPreferencesRepository.GetATSConnStrings(clientIntegration.MOTUClientId);
                    _logger.LogInformation($"{clients.Count} Clients with MOTUClientID: {clientIntegration.MOTUClientId}");
                    int countC = 0;
                    foreach (var client in clients)
                    {
                        _logger.LogInformation($"Processing starts({++countC}/{clients.Count}) for ClientDeploymentID:{client.ClientDeploymentID}, {client.ApplicationName}, MOTUClientID: {clientIntegration.MOTUClientId}, APIKey: {clientIntegration.APIKey}");

                        var contextOptions = new DbContextOptionsBuilder<AppDbContext>()
                                            .UseSqlServer(client.Value)
                                            .Options;

                        _logger.LogInformation($"ConnStr:{client.Value}");
                        using (var context = new AppDbContext(contextOptions))
                        {
                            VideoInterviewRepository _videoInterviewRepository = new VideoInterviewRepository(context);

                            _logger.LogInformation($"Getting VideoInterviews, from last {pastDaysCount} days!");
                            var videoInterviewsCompleted = await _videoInterviewRepository.GetCompletedVideoInterviews(pastDaysCount);
                            _logger.LogInformation($"{videoInterviewsCompleted.Count} completed VideoInterviews for ClientDeploymentID:{client.ClientDeploymentID}");
                            int countVIC = 0;
                            foreach (var videoInterviewItem in videoInterviewsCompleted)
                            {
                                _logger.LogInformation($"Completed interview ({++countVIC}/{videoInterviewsCompleted.Count}) for Applicant: " +
                                                         $"{client.ClientDeploymentID}-{videoInterviewItem.Interview.ApplicantProfileId}, " +
                                                         $"completedDate:{videoInterviewItem.DateTimeCompleted}, JobId: {videoInterviewItem.Interview.JobId} " +
                                                        $"{videoInterviewItem.Interview.ApplicantProfile.FirstName} {videoInterviewItem.Interview.ApplicantProfile.MiddleName} {videoInterviewItem.Interview.ApplicantProfile.OtherName} {videoInterviewItem.Interview.ApplicantProfile.LastName}, " +
                                                        $"Email: {videoInterviewItem.Interview.ApplicantProfile.Email}");
                            }

                            var videoInterviews = await _videoInterviewRepository.GetIncompleteVideoInterviews(pastDaysCount);
                            _logger.LogInformation($"{videoInterviews.Count} incomplete VideoInterviews for ClientDeploymentID:{client.ClientDeploymentID}");
                            int countVI = 0;
                            foreach (var videoInterview in videoInterviews)
                            {
                                _logger.LogInformation($"Processing for({++countVI}/{videoInterviews.Count}) Applicant: " +
                                                         $"{client.ClientDeploymentID}-{videoInterview.Interview.ApplicantProfileId}, JobId: {videoInterview.Interview.JobId}, IntegrationInterviewID(DocumentID): {videoInterview.Integration_InterviewID.ToString()} " +
                                                        $"{videoInterview.Interview.ApplicantProfile.FirstName} {videoInterview.Interview.ApplicantProfile.MiddleName} {videoInterview.Interview.ApplicantProfile.OtherName} {videoInterview.Interview.ApplicantProfile.LastName}, " +
                                                        $"Email: {videoInterview.Interview.ApplicantProfile.Email}");
                                VideoInterview videoInterviewMOTU = _videoInterviewService.GetUuid(videoInterview.Integration_InterviewID.ToString());
                                if (videoInterviewMOTU != null)
                                {
                                    _logger.LogInformation($"{client.ClientDeploymentID}-{videoInterview.Interview.ApplicantProfileId}: InterviewID:{videoInterview.Interview.InterviewId}-TBD by:{videoInterview.Interview.AvailableBefore}, Received UUID: {videoInterviewMOTU.Uuid} for Integration_InterviewID: {videoInterview.Integration_InterviewID}");
                                    //make the api call
                                    var videoInterviewResult = await _sparkHireService.GetStatusFromSparkHireAsync(videoInterviewMOTU.Uuid, clientIntegration.APIKey);
                                    if (videoInterviewResult.Status.Equals("completed"))
                                    {
                                        _logger.LogInformation($"{client.ClientDeploymentID}-{videoInterview.Interview.ApplicantProfileId}: Update VideoInterview_Interviews and Interview_Interviews");
                                        videoInterview.Integration_InterviewStatus = "Completed";
                                        videoInterview.DateTimeCompleted = videoInterviewResult.CompletedDate;
                                        videoInterview.Interview.ScheduleStatus = "Accepted";
                                        videoInterview.Interview.InterviewDate = videoInterviewResult.CompletedDate?.ToString("yyyyMMdd");
                                        await _videoInterviewRepository.Update(videoInterview);

                                        _logger.LogInformation($"{client.ClientDeploymentID}-{videoInterview.Interview.ApplicantProfileId}: Insert Interview_Results");
                                        InterviewResultRepository _interviewResultRepository = new InterviewResultRepository(context);
                                        Interview_Result interviewResult = new Interview_Result();
                                        interviewResult.InterviewID = videoInterview.Interview.InterviewId;
                                        interviewResult.ApplicantProfileID = videoInterview.Interview.ApplicantProfileId;
                                        interviewResult.JobID = videoInterview.Interview.JobId;
                                        await _interviewResultRepository.AddAsync(interviewResult);

                                    }
                                    else
                                    {
                                        _logger.LogInformation($"{client.ClientDeploymentID}-{videoInterview.Interview.ApplicantProfileId}: Interview Incomplete. Current status: {videoInterviewResult.Status}");
                                    }
                                }
                                else
                                {
                                    _logger.LogInformation($"Details no found in MOTU VideoInterview!");
                                }
                            }
                        }

                        _logger.LogInformation($"Processing ends for ClientDeploymentID:{client.ClientDeploymentID}, {client.ApplicationName}, MOTUClientID: {clientIntegration.MOTUClientId}, APIKey: {clientIntegration.APIKey}");
                    }

                    _logger.LogInformation($"Processing ends for MOTUClientID: {clientIntegration.MOTUClientId}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"EXCEPTION Message: {ex.Message}");
                _logger.LogInformation($"EXCEPTION Type: {ex.GetType()}");
                _logger.LogInformation($"INNER Exceptio: {ex.InnerException}");
                _logger.LogInformation($"STACKTRACE: {ex.StackTrace}");
                return false;
            }
            return true;
        }
    }
}
