﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Models.MOTU
{
    public interface IMOTUMongoDatabaseSettings
    {
        string ClientIntegrationCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string VideoInterviewCollectionName { get; set; }
    }

    public class MOTUMongoDatabaseSettings : IMOTUMongoDatabaseSettings
    {
        public string VideoInterviewCollectionName { get; set; }
        public string ClientIntegrationCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }

    }
}
