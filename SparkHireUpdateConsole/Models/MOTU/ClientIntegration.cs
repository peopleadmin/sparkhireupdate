﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Models.MOTU
{
    [BsonIgnoreExtraElements]
    public class ClientIntegration
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedUserId { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedUserId { get; set; }

        public bool Deleted { get; set; }

        [BsonElement("ClientId")]
        public string MOTUClientId { get; set; }
        public string IntegrationId { get; set; }
        public string APIKey { get; set; }
        public string ProductClientId { get; set; }
        public string ProductClientUrl { get; set; }
    }
}
