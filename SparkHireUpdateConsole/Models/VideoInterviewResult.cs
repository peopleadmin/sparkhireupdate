﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Models
{
    public class VideoInterviewResult
    {
        public string Status { get; set; }
        public DateTime? CompletedDate { get; set; }
    }
}
