﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Entities.OnDemand
{
    public class ClientDetail
    {
        public int ClientDeploymentID { get; set; }
        public string ApplicationName { get; set; }
        public string KeyName { get; set; }
        public string Value { get; set; }
    }
}
