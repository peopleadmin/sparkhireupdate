﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Entities.ATS
{
    public class Job
    {
        public int JobId { get; set; }
        public string Title { get; set; }
    }
}
