﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SparkHireUpdateData.Entities.ATS
{
    [Table("VideoInterview_Interviews")]
    public class VideoInterview_Interview
    {
        [Key]
        public int VideoInterviewID { get; set; }
        public int InterviewID { get; set; }
        public Interview_Interview Interview { get; set; }
        public Guid Integration_InterviewID { get; set; }
        public string Integration_InterviewStatus { get; set; }
        public bool Deactivated { get; set; }
        public DateTime? DeactivatedDateTime { get; set; }
        public DateTime DateTimeScheduled { get; set; }
        public DateTime? DateTimeCompleted { get; set; }
        public DateTime Metric_DateTimeCreated { get; set; }

    }
}
