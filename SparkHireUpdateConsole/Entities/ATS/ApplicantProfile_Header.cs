﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Entities.ATS
{
    [Table("ApplicantProfile_Header")]
    public class ApplicantProfile_Header
    {
        [Key]
        public int ApplicantProfileID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string OtherName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
