﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SparkHireUpdateData.Entities.ATS
{
    [Table("Interview_Results")]
    public class Interview_Result
    {
        [Key]
        public int InterviewResultsID { get; set; }
        public int InterviewID { get; set; }
        public Interview_Interview Interview { get; set; }
        public int ApplicantProfileID { get; set; }
        public int JobID { get; set; }
        public int RatingById { get; set; }
        public int Metric_DateTimeCreatedUTC { get; set; } 

    }
}
