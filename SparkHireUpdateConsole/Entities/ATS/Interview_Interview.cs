﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Entities.ATS
{
    [Table("Interview_Interviews")]
    public class Interview_Interview
    {
        [Key]
        public int InterviewId { get; set; }
        public int ApplicantProfileId { get; set; }
        public ApplicantProfile_Header ApplicantProfile { get; set; }
        public int JobId { get; set; }
        public Job Job { get; set; }
        public string InterviewDate { get; set; }
        public string ScheduleStatus { get; set; }
        public string AvailableBefore { get; set; }
    }
}
