﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SparkHireUpdateData.Entities.OnDemand;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateConsole.Persistence.Configurations
{
    public class ClientDetailConfiguration : IEntityTypeConfiguration<ClientDetail>
    {
        public void Configure(EntityTypeBuilder<ClientDetail> builder)
        {
            builder.HasNoKey();
        }
    }
}
