﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Persistence.Configurations
{
    public class JobConfiguration : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.HasAnnotation("Table", "Jobs")
                .HasKey(j=>j.JobId);
        }
    }
}
