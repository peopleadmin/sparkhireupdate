﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Persistence.Configurations
{
    public class ApplicantProfileConfiguration : IEntityTypeConfiguration<ApplicantProfile>
    {
        public void Configure(EntityTypeBuilder<ApplicantProfile> builder)
        {
            builder.HasAnnotation("Table", "ApplicantProfile_Header")
                .HasKey(a=>a.ApplicantProfileID);
        }
    }
}
