﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Persistence.Configurations
{
    public class InterviewResultConfiguration : IEntityTypeConfiguration<InterviewResult>
    {
        public void Configure(EntityTypeBuilder<InterviewResult> builder)
        {
            builder.HasAnnotation("Table", "Interview_Results")
                .HasKey(i=>i.InterviewResultsID); 
        }
    }
}
