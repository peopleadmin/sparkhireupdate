﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Persistence.Configurations
{
    public class VideoInterviewConfiguration : IEntityTypeConfiguration<VideoInterview>
    {
        public void Configure(EntityTypeBuilder<VideoInterview> builder)
        {
            builder.HasAnnotation("Table", "VideoInterview_Interviews")
                .HasKey(v=>v.VideoInterviewID);
        }
    }
}
