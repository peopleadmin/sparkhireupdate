﻿using Microsoft.EntityFrameworkCore;
using SparkHireUpdateData.Entities.OnDemand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public class ClientDetailRepository : IClientDetailRepository
    {
        private readonly OnDemandDbContext _onDemandDbContext;

        public ClientDetailRepository(OnDemandDbContext onDemandDbContext)
        {
            _onDemandDbContext = onDemandDbContext;
        }

        public async Task<List<ClientDetail>> GetATSConnStrings(string MOTUClientID)
        {
            var clientDetails = await _onDemandDbContext.ClientPreferences
                            .FromSqlRaw($"select CP1.ClientDeploymentID, CD1.ApplicationName, CP1.KeyName, CP1.Value " +
                            $" from ClientPreferences as CP1 inner join ClientDeployment as CD1 " +
                            $"on CP1.ClientDeploymentID=CD1.ClientDeploymentID " +
                            $"where CP1.ClientDeploymentID=" +
                            $"(select top 1 CD.ClientDeploymentID " +
                            $"from ClientPreferences CP join ClientDeployment CD " +
                            $"on CD.ClientDeploymentID = CP.ClientDeploymentID " +
                            $"where CP.KeyName = 'MOTU_ClientID' and CP.Value ='{MOTUClientID}' " +
                            $"and CD.ProductID = 2) and CP1.KeyName = 'SYSTEM_DBCONNECTION'")
                            .ToListAsync();

            return clientDetails;
        }
    }
}
