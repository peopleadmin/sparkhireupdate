﻿using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public interface IVideoInterviewRepository /*: IAsyncRepository<VideoInterview>*/
    {
        Task<List<VideoInterview>> GetIncompleteVideoInterviews(int pastDaysCount);
    }
}
