﻿using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public interface IInterviewResultRepository:IAsyncRepository<InterviewResult>
    {
    }
}
