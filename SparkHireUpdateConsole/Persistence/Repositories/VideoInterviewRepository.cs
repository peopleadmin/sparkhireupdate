﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SparkHireUpdateData.Entities.ATS;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public class VideoInterviewRepository  
    {
        private readonly AppDbContext _appDbContext;

        public VideoInterviewRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<List<VideoInterview_Interview>> GetIncompleteVideoInterviews(int pastDaysCount)
        {
            var videoInterviews = await _appDbContext.VideoInterviews
                                        .Where(v => v.Integration_InterviewStatus.ToUpper() != "COMPLETED"
                                                 && v.Metric_DateTimeCreated.Date > DateTime.Now.AddDays(-pastDaysCount))
                                        .Include(vi=>vi.Interview)
                                        .ThenInclude(i=>i.ApplicantProfile)
                                        .ToListAsync();

            return videoInterviews;
        }

        public async Task<List<VideoInterview_Interview>> GetCompletedVideoInterviews(int pastDaysCount)
        {
            var videoInterviews = await _appDbContext.VideoInterviews
                                        .Where(v => v.Integration_InterviewStatus.ToUpper() == "COMPLETED"
                                                 &&  v.DateTimeCompleted.HasValue && (v.DateTimeCompleted.Value.Date > DateTime.Now.AddDays(-pastDaysCount)))
                                        .Include(vi => vi.Interview)
                                        .ThenInclude(i => i.ApplicantProfile)
                                        .ToListAsync();

            return videoInterviews;
        }

        public async Task<VideoInterview_Interview> Update(VideoInterview_Interview videoInterview)
        {
            _appDbContext.Entry(videoInterview).State = EntityState.Modified;
            await _appDbContext.SaveChangesAsync();

            return videoInterview;
        }
    }
}
