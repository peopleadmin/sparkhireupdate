﻿using SparkHireUpdateData.Entities.OnDemand;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public interface IClientDetailRepository
    {
        Task<List<ClientDetail>> GetATSConnStrings(string MOTUClientID);
    }
}