﻿using Microsoft.EntityFrameworkCore;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public class InterviewRepository 
    {
        private readonly AppDbContext _appDbContext;

        public InterviewRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }    

        public async Task<Interview_Interview> GetById(int id)
        {
            var interviewResult = await _appDbContext.Interviews.AsNoTracking()
                                            .SingleOrDefaultAsync(b => b.InterviewId == id);
            return interviewResult;
        }

        
        public async Task<Interview_Interview> Update(Interview_Interview interview)
        {
            _appDbContext.Entry(interview).State = EntityState.Modified;
            await _appDbContext.SaveChangesAsync();

            return interview;
        }

    }
}
