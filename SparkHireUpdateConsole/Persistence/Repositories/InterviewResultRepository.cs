﻿using Microsoft.EntityFrameworkCore;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SparkHireUpdateData.Persistence.Repositories
{
    public class InterviewResultRepository  
    {
        private readonly AppDbContext _appDbContext;

        public InterviewResultRepository(AppDbContext appDbContext)  
        {
            _appDbContext = appDbContext;
        }

        public async Task<Interview_Result> GetById(int id)
        {
            var interviewResult = await _appDbContext.InterviewResults.AsNoTracking()
                                            .SingleOrDefaultAsync(b => b.InterviewResultsID == id);
            return interviewResult;
        }


        public async Task<Interview_Result> Update(Interview_Result interviewResult)
        {
            _appDbContext.Entry(interviewResult).State = EntityState.Modified;
            await _appDbContext.SaveChangesAsync();

            return interviewResult;
        }


        public async Task<Interview_Result> AddAsync(Interview_Result interviewResult)
        {
            await _appDbContext.InterviewResults.AddAsync(interviewResult);
            await _appDbContext.SaveChangesAsync();

            return interviewResult;
        }

    }
}
