﻿using Microsoft.EntityFrameworkCore;
using SparkHireUpdateData.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkHireUpdateData.Persistence
{
    public class AppDbContext : DbContext
    {
        //private readonly string _connectionString;

        //public AppDbContext(string connectionString)
        //{
        //    _connectionString = connectionString;
        //}

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(_connectionString);//.LogTo(Console.WriteLine).EnableSensitiveDataLogging();
        //}

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<ApplicantProfile_Header> ApplicantProfiles { get; set; }
        public DbSet<Interview_Interview> Interviews { get; set; }
        public DbSet<Interview_Result> InterviewResults { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<VideoInterview_Interview> VideoInterviews { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
                                        => optionsBuilder.LogTo(Console.WriteLine);
                                             //.EnableSensitiveDataLogging();

    }
}

